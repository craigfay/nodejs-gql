/**
 * Tests for entities and use cases
 */

/**
 * Dependencies
 */
const QuizEntity = require('../domain/entities/quiz_entity');
const SubmissionEntity = require('../domain/entities/submission_entity');
const useCases = require('../domain/entities/use_cases');

test('Quizzes can be opened and closed', () => {
    // Create an empty quiz entity
    let quiz1 = new QuizEntity('', '', '', [], false);
    expect(quiz1.open).toBe(false);

    // Open it with the use cases function
    useCases.openQuiz(quiz1);
    expect(quiz1.open).toBe(true);

    // Create an open quiz
    const quiz2 = new QuizEntity('', '', '', [], true);
    expect(quiz2.open).toBe(true);

    // Close it with the use cases function
    useCases.closeQuiz(quiz2);
    expect(quiz2.open).toBe(false);
});

test('Quizzes can be graded against submissions', () => {
    // Create questions and answers
    const testQuestions = [
        {correctAnswers: ['Sam', 'sam']},
        {correctAnswers: ['I', 'i']},
        {correctAnswers: ['Am', 'am']}
    ];
    const testAnswers = ['Sam','I','Am'];
    // Create a quiz and a submission
    const quiz = new QuizEntity('', '', '', testQuestions, false);
    const submission = new SubmissionEntity('testUser', testAnswers);
    // Check the result of grading
    expect(useCases.gradeQuiz(quiz, submission)).toEqual([true, true, true]);
})