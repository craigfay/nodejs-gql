/*
 * Server configuration variables
 *
 */

// Export Wrapper 
const options = {};

/*
 * Database variables
 */
options.db = {
    prefix: 'mongodb',
    address: 'localhost',
    port: '27017',
    name: 'test_db',
};
options.db.url = `${options.db.prefix}://${options.db.address}:${options.db.port}/${options.db.name}`;

// Used to hash user passwords
options.hashingSecret = 'How is a raven like a writing desk?';

// If the PORT environment variable is set, use it for the port. Default to 3000.
options.port = process.env.PORT || 3000;

module.exports = options;
