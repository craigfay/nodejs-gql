/**
 * Primary server module
 */

/**
 * Dependencies
 */
const config = require('./config');
const server = require('./boundary/web/server');
const database = require('./boundary/persistence/database');

/**
 * Open External Boundaries
 */
server.init(config);
database.init(config);
