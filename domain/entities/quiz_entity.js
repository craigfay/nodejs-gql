/**
 * An application agnostic business entity 
 * @todo find a more elegant way to write grade()
 */

class QuizEntity {
    constructor(name=String, description=String, author=String, questions=Array, open=Boolean) {
        this.name = name;
        this.description = description;
        this.author = author;
        this.questions = questions;
        this.open = open;
    }
}

module.exports = QuizEntity;
