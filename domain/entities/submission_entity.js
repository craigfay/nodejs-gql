/**
 * An application independant business entity
 */

class SubmissionEntity {
    constructor(submitter=String, answers=Array) {
        this.submitter = submitter;
        this.answers = answers;
    }
}

module.exports = SubmissionEntity;
