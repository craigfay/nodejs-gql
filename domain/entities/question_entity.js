/**
 * An application agnostic business entity 
 */

class QuestionEntity {
    constructor(format=String, body=String, correctAnswers=Array, value=Number) {
        this.format = format;
        this.body = body;
        this.correctAnswers = correctAnswers;
        this.value = value;
    }
}

module.exports = QuestionEntity;
