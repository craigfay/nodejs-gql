/**
 * Application agnostic use cases
 * @todo A response model consists of data created by the interactor as the result of a use case
 */

// Export Wrapper
const useCases = {};

useCases.gradeQuiz = (quiz, submission) => {
    // an array of correct answers for the quiz
    const key = quiz.questions.map(question => question.correctAnswers);

    let results = [];
    for (let i = 0; i < key.length; i++) {
        // true if the current answer is included in the current correct answers
        results.push(key[i].includes(submission.answers[i]));
    }
    return results;
}

useCases.openQuiz = (quiz) => {
    quiz.open = true;
}

useCases.closeQuiz = (quiz) => {
    quiz.open = false;
}

module.exports = useCases;
