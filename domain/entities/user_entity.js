/**
 * An application agnostic business entity 
 */

class UserEntity {
    constructor(username=String, password=String, email=String) {
        this.username = username;
        this.password = password;
        this.email = email;
    }
}

module.exports = UserEntity;
