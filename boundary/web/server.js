/**
 * A Boundary Object that opens communication with the web
 * 
 */

/**
 * Dependencies
 */
const express = require('express');
const graphQLHTTP = require('express-graphql');
const graphQLSchema = require('./graphql/schema');

/**
 * Export Wrapper
 */
const server = {};

/** @function server.init
 * Start listening for web requests 
 * @param {Object} config - A configuration object with a port property 
 */
server.init = (config) => {
    /**
     * The 'Express App' 
     */
    const app = express();

    /**
     * Middleware
     */
    app.use('/graphql', graphQLHTTP({
        schema: graphQLSchema,
        graphiql: true
    }));

    /**
     * Start the server
     */
    app.listen(config.port, () => {
        console.log(`Server listening on port ${config.port} ...`)
    });
}

module.exports = server;
