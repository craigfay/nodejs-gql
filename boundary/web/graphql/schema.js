/**
 * This module defines types, and relationships between types.
 * It also defines root queries, or initial entries into the graph.
 * @todo Add a question type
 * @todo Enforce business rules
 */

/**
 * Dependencies
 */
const User = require.main.require('./boundary/persistence/models/mongoose/user');
const Quiz = require.main.require('./boundary/persistence/models/mongoose/quiz');
const Question = require.main.require('./boundary/persistence/models/mongoose/question');
const Submission = require.main.require('./boundary/persistence/models/mongoose/submission');
const graphql = require('graphql');

/**
 * Destructured GraphQL Properties
 */
const {
    GraphQLObjectType,
    GraphQLString,
    GraphQLSchema,
    GraphQLID,
    GraphQLBoolean,
    GraphQLList,
} = graphql;

const UserType = new GraphQLObjectType({
    name: 'user',
    // fields must be a function to prevent 'undefined' errors
    fields: () => ({
        id: {type: GraphQLID},
        username: {type: GraphQLString},
        password: {type: GraphQLString},
        email: {type: GraphQLString},
        quizzes: {
            type: new GraphQLList(QuizType),
            resolve(parent, args) {
                return Quiz.find({author: parent.username});
            }
        }
    })
});

const QuizType = new GraphQLObjectType({
    name: 'quiz',
    fields: () => ({
        id: {type: GraphQLID},
        name: {type: GraphQLString}, 
        author: {
            type: UserType,
            resolve(parent, args) {
                return User.findOne({username: parent.author});
            }
        },
        description: {type: GraphQLString},
        questions: {
            type: new GraphQLList(QuestionType),
            resolve(parent, args) {
                return Question.find({'_id': { $in: parent.questions}});
            }
        },
        open: {type: GraphQLBoolean}
    })
});

const QuestionType = new GraphQLObjectType({
    name: 'question',
    fields: () => ({
        id: {type: GraphQLID},
        format: {type: GraphQLString},
        body: {type: GraphQLString},
        correctAnswers: {type: new GraphQLList(GraphQLString)},
        open: {type: GraphQLBoolean}
    })
});

const RootQuery = new GraphQLObjectType({
    name: 'RootQueryType',
    fields: () => ({
        user: {
            type: UserType,
            args: {username: {type: GraphQLString}},
            resolve(parent, args) {
                // Return the result of a database query
                return User.findOne({username: args.username});
            }
        },
        quiz: {
            type: QuizType,
            args: {
                author: {type: GraphQLString},
                name: {type: GraphQLString},
            },
            resolve(parent, args) {
                // Return the result of a database query
                return Quiz.findOne({author: args.author, name: args.name})
            }
        },
        question: {
            type: QuestionType,
            args: {
                id: {type: GraphQLID},
            },
            resolve(parent, args) {
                return Question.findOne({id: args.id});
            }
        },
        users: {
            type: new GraphQLList(UserType),
            resolve(parent, args) {
                // Return the result of a database query
                return User.find({});
            }
        },
        quizzes: {
            type: new GraphQLList(QuizType),
            resolve(parent, args) {
                // Return the result of a database query
                return Quiz.find({});
            }
        },
        questions: {
            type: new GraphQLList(QuestionType),
            resolve(parent, args) {
                return Question.find({});
            }
        }
    })
});

const Mutation = new GraphQLObjectType({
    name: 'Mutation',
    fields: {
        addUser: {
            type: UserType,
            args: {
                username: {type: GraphQLString},
                password: {type: GraphQLString},
                email: {type: GraphQLString},
            },
            resolve(parent, args) {
                const user = new User({
                    username: args.username,
                    password: args.password,
                    email: args.email
                });
                return user.save();
            }
        },
        addQuiz: {
            type: QuizType,
            args: {
                name: {type: GraphQLString},
                author: {type: GraphQLString},
                description: {type: GraphQLString},
                questions: {type: new GraphQLList(GraphQLID)},
                open: {type: GraphQLBoolean}
            },
            resolve(parent, args) {
                const quiz = new Quiz({
                    name: args.name,
                    author: args.author,
                    description: args.description,
                    questions: args.questions,
                    open: args.open
                });
                return quiz.save();
            }
        },
        addQuestion: {
            type: QuestionType,
            args: {
                format: {type: GraphQLString},
                body: {type: GraphQLString},
                correctAnswers: {type: new GraphQLList(GraphQLString)}
            },
            resolve(parent, args) {
                const question = new Question({
                    format: args.format,
                    body: args.body,
                    correctAnswers: args.correctAnswers
                });
                return question.save();
            }
        }
    }
});

module.exports = new GraphQLSchema({
    query: RootQuery,
    mutation: Mutation
});
