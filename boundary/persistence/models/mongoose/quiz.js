/**
 * Model for mongo quiz documents via mongoose
 * 
 */

/**
 * Dependencies
 */
const mongoose = require('mongoose');
const QuizEntity = require.main.require('./domain/entities/quiz_entity');

/**
 * Export
 */
const QuizSchema = new mongoose.Schema(new QuizEntity())
module.exports = mongoose.model('Quiz', QuizSchema);
 