/**
 * Model for mongo quuestion documents via mongoose
 * 
 * @todo directly require entities
 */

/**
 * Dependencies
 */
const mongoose = require('mongoose');
const QuestionEntity = require.main.require('./domain/entities/question_entity');

/**
 * Export
 */
const QuestionSchema = new mongoose.Schema(new QuestionEntity())
module.exports = mongoose.model('Question', QuestionSchema);