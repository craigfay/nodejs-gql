/**
 * Model for mongo user documents via mongoose
 * 
 */

 /**
  * Dependencies
  */
const mongoose = require('mongoose');
const UserEntity = require.main.require('./domain/entities/user_entity')

/**
 * Export
 */
const UserSchema = new mongoose.Schema(new UserEntity())
module.exports = mongoose.model('User', UserSchema);
