/**
 * Model for mongo submission documents via mongoose
 * 
 */

/**
 * Dependencies
 */
const mongoose = require('mongoose');
const SubmissionEntity = require.main.require('./domain/entities/submission_entity');

/**
 * Export
 */
const SubmissionSchema = new mongoose.Schema(new SubmissionEntity())
module.exports = mongoose.model('Submission', SubmissionSchema);