/**
 * A Boundary Object that opens communication with a database
 * 
 */

/**
 * Dependencies
 */
const mongoose = require('mongoose');

/**
 * Export Wrapper
 */
const database = {};

/** @function database.init
 * Open a connection with a MongoDB Database 
 * @param {Object} config - A configuration object with a port property 
 */
database.init = (config) => {
    /**
     * Connect to the database
     */
    mongoose.connect(config.db.url, { useNewUrlParser: true });
    mongoose.connection.once('open', () => {
        console.log(`Database connected at ${config.db.url} ...`);
    });
}

module.exports = database;
